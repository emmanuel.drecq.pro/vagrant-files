#!/usr/bin/python3

import sys,os,click,random,string,json,datetime,time,yaml
import pprint
import elasticsearch as elastic
from tabulate import tabulate
import libs.elastic_querier as eq

"""
Channel examples : cocadmin, AntonPutra, RahulWagh, AndrejBaranovskij, LearnWithArif, wenkatn-justmeandopensource
"""


yaml_settings = dict()


@click.command()
@click.option('--lst', '-l', is_flag=True, help="List all channels")
@click.option('--pause', '-p', is_flag=True, help="Pause scraping")
@click.option('--unpause', '-u', is_flag=True, help="Unpause scraping")
@click.option('--add', '-a', help="Add a new channel")
@click.option('--config', '-C', help="config location")
@click.option('--delete', '-d', help="Delete a channel")
@click.option('--waiting', '-w', help="Update status of a channel")
def cli(lst,add,delete,waiting,pause,unpause,config):

  if config:

    if os.path.isfile(config):
    
      with open(config, "r") as ymlfile:
        yaml_settings = yaml.load(ymlfile, Loader=yaml.FullLoader)
    
      scheme = yaml_settings['elasticsearch']['scheme']
      host   = yaml_settings['elasticsearch']['host']
      port   = yaml_settings['elasticsearch']['port']
      index  = yaml_settings['elasticsearch']['index']
    
    else:
     
      print("Erreur - no config.yaml file or environement variables")
      sys.exit(1)


  if lst:
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    print('\n### Channels list ###\n')
    channels = eq.queryElastic(es_api,index)
   
    lst_tabulate = []
    for hit in channels['hits']['hits']:
      doc = hit['_source']
      lst_tabulate.append([doc['name'],doc['status'],doc['createdAt']])
    print(tabulate(lst_tabulate, headers=['Name', 'Age','CreatedAt']))
    print('\nTotal Channels : ' + str(channels['hits']['total']['value']) + '\n')

  if add:
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    eq.insertChannel(es_api,index,add)

  if delete:
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    eq.removeChannel(es_api,index,delete)

  if waiting:
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    eq.setStatusChannel(es_api,index,waiting,'waiting')

  if pause:
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    channels = eq.queryElastic(es_api,index)
    print("Waiting for pause...")
    for hit in channels['hits']['hits']:
      doc = hit['_source']
      eq.setStatusChannel(es_api,index,doc['name'],'pause')
    print("All channels paused")

  if unpause:
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    channels = eq.queryElastic(es_api,index)
    print("Waiting for unpause...")
    for hit in channels['hits']['hits']:
      doc = hit['_source']
      eq.setStatusChannel(es_api,index,doc['name'],'waiting')
    print("All channels unpaused")

if __name__ == '__main__':
  try:
    cli()
    os._exit(0)
  except:
    os._exit(1)

