#!/usr/bin/python3

import pika, sys, os, click
import json,yaml
import time
import elasticsearch as elastic
import libs.elastic_querier as eq
import logging

def inserter(scheme,host,port,index,rmq_host,rmq_queue,verbose):

  if verbose:
    logging.basicConfig(format="{'level':'%(levelname)s','message':'%(message)s'}", level=logging.DEBUG)
  else:
    logging.basicConfig(format="%(levelname)s: %(message)s")


  connection = pika.BlockingConnection(pika.ConnectionParameters(host=rmq_host))
  channel = connection.channel()
  channel.queue_declare(queue = rmq_queue, durable = True)

  def callback(ch, method, properties, body):
    data = json.loads(body)
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    register = es_api.index(index = index, id = data['href'], document = body)
    logging.debug(register)
  channel.basic_consume(queue=rmq_queue, on_message_callback=callback, auto_ack=True)
  channel.start_consuming()

@click.command()
@click.option('--run', '-r', is_flag=True, help="run scraper")
@click.option('--verbose', '-v', is_flag=True, help="run scraper")
@click.option('--config', '-C', help="config location")
def cli(run,config,verbose):
  if config:

    if os.path.isfile(config):
    
      with open(config, "r") as ymlfile:
        yaml_settings = yaml.load(ymlfile, Loader=yaml.FullLoader)
    
      scheme = yaml_settings['elasticsearch']['scheme']
      host   = yaml_settings['elasticsearch']['host']
      port   = yaml_settings['elasticsearch']['port']
      index  = yaml_settings['elasticsearch']['index']

      rmq_host    = yaml_settings['rabbitmq']['host']
      rmq_queue   = yaml_settings['rabbitmq']['queue']
    
    else:
      print("Erreur - no config.yaml file or environement variables")
      sys.exit(1)
  
  if run:
    inserter(scheme,host,port,index,rmq_host,rmq_queue,verbose)    
 
if __name__ == '__main__':
  try:
    cli()
  except KeyboardInterrupt:
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)


